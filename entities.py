"""
Contains entity logic.
"""

from sys import modules

from inspect import getmembers, isclass
from logger import get_logger
from states import State
log = get_logger()

# todo low priority: honestly class organization is kind of shoddy


def _construct_item_class_map():
    """Build a dictionary mapping string names (in natural language) to classes."""
    ic_map = {}
    class_list = [m[1] for m in getmembers(modules[__name__], isclass) if m[1].__module__ == 'entities']
    for cl in class_list:
        if hasattr(cl, 'names'):
            for name in cl.names:
                ic_map[name] = cl
    return ic_map


def item2class(item_name):
    """Converts item names to class definitions."""
    item_name = item_name.lower()
    return _item_class_map[item_name] if item_name in _item_class_map else None


class Item(object):
    """Base class for all objects in the game world that can be interacted with."""
    names = []  # todo list instead of set ?

    def __init__(self):
        self.active_traits = set()  # set of classes
        # "active" for Drinkable means the item has been drunk; for Lockable it means the item has been locked, etc.

    def activate(self, trait):
        self.active_traits.add(trait)

    def deactivate(self, trait):
        if self.is_active(trait):
            self.active_traits.remove(trait)

    def get_state(self):
        return State.ITEM_STATELESS

    def is_active(self, trait=None):
        if trait is None:
            return True if len(self.active_traits) > 0 else False
        return True if trait in self.active_traits else False

    @classmethod
    def canonical_name(cls):
        return cls.names[0] if len(cls.names) > 0 else 'item'

    # todo can compress the following class methods into one, with an additional argument
    @classmethod
    def can_access_internet(cls):
        return True if issubclass(cls, InternetEnabledItem) else False

    @classmethod
    def can_be_drunk(cls):
        return True if issubclass(cls, DrinkableItem) else False

    @classmethod
    def can_be_grabbed(cls):
        return True if issubclass(cls, GrabbableItem) else False

    @classmethod
    def can_be_hung(cls):
        return True if issubclass(cls, HangableItem) else False

    @classmethod
    def can_be_locked(cls):
        return True if issubclass(cls, LockableItem) else False

    @classmethod
    def can_be_opened(cls):
        return True if issubclass(cls, OpenableItem) else False

    @classmethod
    def can_be_sat_upon(cls):
        return True if issubclass(cls, SittableItem) else False

    @classmethod
    def can_be_worn(cls):
        return True if issubclass(cls, WearableItem) else False

    @classmethod
    def has_inventory(cls):
        return True if issubclass(cls, ContainerItem) else False

    @classmethod
    def is_electronic(cls):
        return True if issubclass(cls, ElectronicItem) else False

    @classmethod
    def is_password_protected(cls):
        return True if issubclass(cls, PasswordProtectedItem) else False


class ContainerItem(Item):
    def __init__(self):
        super().__init__()
        self.inventory = set()

    def add_to_inventory(self, item):
        self.inventory.add(item)

    def remove_from_inventory(self, item):
        self.inventory.remove(item)

    def get_items(self, item_class=None, pop=False):
        items = self.inventory if item_class is None else set()
        if item_class is not None:
            items = [i for i in self.inventory if isinstance(i, item_class)]
            if pop:
                for item in items:
                    if item in self.inventory:
                        self.inventory.remove(item)
        return items  # todo continue recursively in case of containers in containers

    @staticmethod
    def valid_inventory_items():
        return {GrabbableItem} - ContainerItem.invalid_inventory_items()

    @staticmethod
    def invalid_inventory_items():
        return set()


class HiddenContainerItem(ContainerItem):
    def __init__(self):
        super().__init__()

    def get_items(self, item_class=None, respect_hidden=True, pop=False):
        items = set() if self.is_hidden() and respect_hidden else self.inventory
        if item_class is not None:
            items = [i for i in items if isinstance(i, item_class)]
            if pop:
                for item in items:
                    if item in self.inventory:
                        self.inventory.remove(item)
        return items

    def get_state(self):
        return State.ITEM_DEACTIVE if self.is_hidden() else State.ITEM_ACTIVE

    def is_hidden(self):
        return False


class DrinkableItem(Item):
    pass


class ElectronicItem(Item):
    def get_state(self):
        return State.ITEM_ACTIVE if self.is_active(ElectronicItem) else State.ITEM_DEACTIVE


class GrabbableItem(Item):
    pass


class HangableItem(Item):
    pass


class InternetEnabledItem(ElectronicItem):
    pass


class OpenableItem(HiddenContainerItem):
    def is_hidden(self):
        return True if super().is_hidden() or not self.is_active(OpenableItem) else False


class PasswordProtectedItem(ElectronicItem):
    def __init__(self):
        super().__init__()
        self.activate(PasswordProtectedItem)  # needs authentication on creation by default

    def deactivate(self, trait):
        super().deactivate(trait)
        if (not issubclass(trait, PasswordProtectedItem)) and issubclass(trait, ElectronicItem):
            self.activate(PasswordProtectedItem)  # turning item off means needing new authentication


class SittableItem(Item):
    pass


class WearableItem(Item):
    pass
    # todo one of these can be worn at a time by the user (this entire class is an easter egg; unnecessary for game)


class LockableItem(OpenableItem):
    def __init__(self):
        super().__init__()
        self.activate(LockableItem)
        self.key = KeyToDesk  # todo door will be locked by this. kinda weird and bad

    def is_hidden(self):
        return True if super().is_hidden() or self.is_active(LockableItem) else False


"""================================================="""


class Backpack(HangableItem, OpenableItem, WearableItem):
    names = ['backpack', 'back pack', 'pack', 'bookbag', 'book bag', 'bag', 'messenger bag']


class CeilingFan(HiddenContainerItem, ElectronicItem):  # todo one wing of the fan is slightly lower than the others
    names = ['fan', 'ceiling fan']

    def __init__(self):
        super().__init__()
        self.has_been_activated = False

    def activate(self, trait):
        super().activate(trait)
        if trait == ElectronicItem:
            self.has_been_activated = True

    def is_hidden(self):
        return True if super().is_hidden() or not self.has_been_activated else False

    @staticmethod
    def valid_inventory_items():
        return {Charger} - CeilingFan.invalid_inventory_items()


class Chair(SittableItem):
    names = ['chair', 'desk chair']


class Charger(ElectronicItem, GrabbableItem):
    names = ['charger', 'adapter', 'power adapter']


class CoatRack(ContainerItem):
    names = []  #['rack', 'coat rack', 'hanger', 'coat hanger', 'stand', 'coat stand']

    @staticmethod
    def valid_inventory_items():
        return {HangableItem} - CoatRack.invalid_inventory_items()


class Couch(SittableItem):
    names = ['couch', 'sofa', 'lounge']


class Door(LockableItem):  # todo door is hiddencontaineritem (unintended but probably fine)
    names = ['door', 'exit']  # todo simple fix: make OpenableItem, not LockableItem


class Desk(LockableItem, OpenableItem):
    names = ['desk', 'drawer', 'cabinet', 'desk drawer']


class Fountain(HiddenContainerItem, DrinkableItem):  # todo DiscoverableContentsContainerItem?
    names = ['fountain', 'zen fountain', 'water fountain', 'zen water fountain', 'water']

    def __init__(self):
        super().__init__()
        self.contents_discovered = False

    def get_state(self):
        return State.ITEM_DEACTIVE if len(self.get_items()) == 0 else State.ITEM_ACTIVE

    # todo: drinking the water causes the fountain to break and the player to lose the game??
    # todo: putting the USB in the fountain breaks it and loses the game?

    def is_hidden(self):
        return True if super().is_hidden() or not self.contents_discovered else False

    @staticmethod
    def invalid_inventory_items():
        return {ElectronicItem}


class Jacket(HangableItem, HiddenContainerItem, WearableItem):  # todo DiscoverableContentsContainerItem?
    names = ['jacket', 'overcoat']  # todo coat? interferes with CoatRack

    def __init__(self):
        super().__init__()
        self.contents_discovered = False

    def get_state(self):
        return State.ITEM_DEACTIVE if len(self.get_items()) == 0 else State.ITEM_ACTIVE

    def is_hidden(self):
        return True if super().is_hidden() or not self.contents_discovered else False


class KeyToDesk(GrabbableItem):
    names = ['key', 'keys', 'small key', 'small silver key', 'silver key']

    def __init__(self):
        super().__init__()


class Lamp(ElectronicItem):
    names = ['lamp', 'desk lamp', 'desk light', 'light']


class Laptop(InternetEnabledItem, PasswordProtectedItem):
    names = ['laptop', 'computer']


class Papers(Item):  # todo could be grabbable in the future
    names = ['papers']


class Phone(InternetEnabledItem, GrabbableItem):
    names = ['phone', 'cell phone', 'cellphone', 'cell', 'smartphone', 'smart phone', 'telephone']

    def __init__(self):
        super().__init__()


class Poster(Item):  # todo could be grabbable in the future
    names = ['poster', 'wall poster']


class Textbook(Item):  # todo could be grabbable in the future
    names = ['book', 'textbook', 'text book']

    def __init__(self):
        super().__init__()


class USB(ElectronicItem, GrabbableItem):
    names = ['usb', 'thumb drive', 'usb drive', 'thumbdrive']


class Wine(DrinkableItem):  # todo could be grabbable in the future
    names = ['wine', 'alcohol', 'bottle', 'bottle of wine', 'bottle of alcohol']


class Wug(Item):
    names = ['wug', 'plush', 'stuffed animal', 'plush animal']


_item_class_map = _construct_item_class_map()
