"""
Contains universal codes to uniquely represent errors and other game states.
"""

from enum import IntEnum

from logger import get_logger
log = get_logger()


# todo correctly order intent error values. when multiple errors happen at once, higher values will be shown to the user
#   first. ex) we don't want to say "you can't reach the secret hidden key" before we say
#   "secret key? never heard of it" because that ruins the surprise...


class State(IntEnum):
    GAME_OK = 0             # no script necessary; nothing of note is happening
    GAME_WIN = 1            # did the thing
    GAME_QUIT = 2           # used keyword "quit"
    GAME_LOSE_ALCOHOL = 3   # drank the bottle of wine
    GAME_LOSE_WEB = 4       # surfed the web
    GAME_LOSE_LEFT = 5      # exited the room

    # Assertion: All error intent states must be greater than the OK intent state
    #  #### Greater error values will be shown to user before lesser error values ####
    INTENT_OK = 10  # This must be the lowest intent state

    # action can't be performed because...
    INTENT_ERR_ITEM_ACTIVE = 12         # item is active (will vary based on intent)
    INTENT_ERR_ITEM_DEACTIVE = 13       # item is de-active (will vary based on intent)
    # "active": DrinkableItems have been drunk, OpenableItems have been opened, LockedItems have been locked, etc.
    # "deactive": opposite of whatever verb is in the VerbableItem
    # EX) INTENT_ERR_ITEM_ACTIVE[TurnOnIntent][CeilingFan] = 'The ceiling fan is already on!'

    INTENT_ERR_ITEM_LOCKED = 14             # hacky special case of active; "That doesn't work on a locked item."
    INTENT_ERR_ITEM_AUTHENTICATED = 15      # hacky special case of active: "Item is already authenticated"
    INTENT_ERR_PUZZLE_UNSOLVED = 16         # not yet discovered the password (and similar puzzles)

    INTENT_ERR_PLAYER_IN_AREA = 25          # "you need to be not where you are currently to do that action" (e.g. goto)
    INTENT_ERR_PLAYER_SEATED = 26           # "you can't do that while sitting down"
    INTENT_ERR_PLAYER_NOT_SEATED = 27       # "you can't do that unless you are sitting"
    INTENT_ERR_PLAYER_HAS_ITEM = 28         # "you can't do that when you have %s in your inventory"
    INTENT_ERR_PLAYER_NOT_HAS_ITEM = 29     # "you can't do that unless you have %s in your inventory"
    INTENT_ERR_NOT_REACH_ITEM = 40          # "it's too far away"

    # Intent is supplied with incompatible item arguments (trying to drink a wug makes no sense)
    INTENT_ERR_NONFUNCTION = 50  # specified per intent: "You are unable to obtain moisture from the wug."

    # What are you talking about, User? I don't know what that word you used even means! (OOV, or object undiscovered)
    # These must be the highest intent error states
    INTENT_ERR_AREA_NOT_EXIST = 97          # unknown area
    INTENT_ERR_ITEM_NOT_EXIST = 98          # unknown item
    INTENT_ERR_INTENT_NOT_EXIST = 99        # unknown command (Rasa returns 'None' for Intent name)

    # Item States
    ITEM_STATELESS = 200
    ITEM_ACTIVE = 201
    ITEM_DEACTIVE = 202

    # Location States
    LOC_STATELESS = 300
    # todo e.g.: the desk before and after charger is placed; the ceiling fan is on or off
    # todo could do some fancy addition to represent stacked states
