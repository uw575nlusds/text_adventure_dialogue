## intent:None
- \quit
- are you a bot?
- blah blah blah
- end game
- give up
- help
- how does this game work?
- quit
- quit game
- stop playing
- what's your name?

## intent:browse_web
- can i browse the [web](item)?
- can i look at the [internet](item)?
- i want to browse the [internet](item)
- i'd like to browse the [web](item).
- let's look at the [internet](item).
- open the [internet](item).

## intent:call
- call advisor on phone
- call my advisor for clarification
- call my advisor to ask them about the [dissertation](item) website
- dial advisor's number on the phone
- use phone to call advisor

## intent:close
- can i close the [door](item)?
- close [door](item)
- close [drawer](item)
- close my [door](item)
- close the [desk drawer](item)
- close the [door](item)
- close the [drawer](item)
- please close [door](item)

## intent:drink
- can i drink the [wine](item)?
- drink the [wine](item)
- drink the [zen pool water](item)
- i want to drink from the [zen pool](item).
- i want to drink the [bottle of wine](item)
- i'm going to use the [wine](item).

## intent:enter_password
- can i enter a [password](item)?
- can i try a [password](item)?
- enter the [password](item)
- i want to try a [password](item)
- let's enter the [password](item).
- try a [password](item).

## intent:get_up_off_of
- can i get up from the [chair](item)?
- get up from the [chair](item)
- i get off the [couch](item)
- i get up from the [chair](item).
- i want to get up from the [couch](item).
- i'd like to get up off the [couch](item).

## intent:go_to_destination
- go back to the [desk](area).
- go closer to the [desk](area).
- go stand near the [desk](area) by the wall.
- go to the [desk](area)
- move to the [desk](area) region.
- walk across the room to the [desk](area).
- walk over to the [desk](area)
- go to the [couch](area)
- go to [couch](area)
- go over to [couch](area)
- I want to walk to the [couch](area)
- go to [end table](area)
- go over to the [end table](area)
- go to the [end table](area)
- go to the [coat rack](area)

## intent:hug
- can i hug the [wug](item)?
- hug the [wug](item).
- i'd like to hug the [wug](item).
- i'm going to hug the [wug](item).
- let's hug the [wug](item).
- please hug the [wug](item).

## intent:lock
- can i lock the [door](item)
- lock [door](item)
- lock [drawer](item)
- lock my [door](item)
- lock the [desk drawer](item)
- lock the [door](item)
- lock the [drawer](item)
- please lock [door](item)

## intent:look_at_item
- check out the [key](item).
- examine the [key](item).
- inspect the [key](item)
- look at the [key](item)
- look inside the [drawer](item).
- see if there's anything in the [drawer](item)
- what's on the [desk](area)?
- look at the [USB](item)
- look at the [charger](item)
- look at the [couch](area)
- look at the [end table](area)

## intent:open
- can i open the [door](item)
- can i open the [door](item)?
- can you open [door](item)?
- open [door](item)
- open [drawer](item)
- open my [door](item)
- open the [desk drawer](item)
- open the [door](item)
- open the [drawer](item)
- please open [door](item)
- please open my [door](item)
- please open the [desk drawer](item)
- please open the [door](item)
- please open the [drawer](item)

## intent:put_item_in_inventory
- hold on to the [key](item).
- pick up the [key](item) and put it in my pocket.
- pick up the [key](item).
- put the [key](item) into my pocket.
- take the [key](item) for later.
- take the [key](item).
- take the [USB](item).
- take the [usb stick](item) so I can have it
- take my [charger](item)
- take my [phone](item)
- take the [phone](item)
- put my [phone](item) in my pocket
- pick up [phone](item)
- I want to take the [phone](item)
- please take the [charger](item)
- pick up [charger](item)
- take [charger](item)
- take [laptop charger](item)
- get [phone](item)
- get [charger](item)
- can I pick up the [charger]?
- can you pick up the [phone]?
- please get [USB](item)
- get [usb](item)
- now get [USB](item)
- go get [USB](item)
- go pick up the [phone](item)
- pick up [phone](item)

## intent:sit
- can i sit down at the [couch](item)?
- i sit down on the [couch](item).
- i sit on the [chair](item).
- i want to sit down at the [chair](item).
- i'm going to sit on the [couch](item).
- sit down at the [chair](item).

## intent:turn-on
- power on [lamp](item)
- power on [laptop](item)
- power up [computer](item)
- switch on [computer](item)
- switch on [lamp](item)
- switch on [laptop](item)
- turn my [laptop](item) on
- turn my [phone](item) on
- turn on [computer](item)
- turn on [lamp](item)
- turn on [laptop](item)
- turn on my [laptop](item)
- turn on my [phone](item)
- turn on the [ceiling fan](item)
- turn on the [fan](item)
- turn the [ceiling fan](item) on
- turn the [computer](item) on
- turn the [desk lamp](item) on
- turn the [fan](item) on
- turn the [laptop](item) on
- turn the [light](item) on
- turn the [machine](item) on
- turn the [tablet](item) on

## intent:turn_off
- power down [computer](item)
- power down [laptop](item)
- power off [lamp](item)
- power off [laptop](item)
- switch off [computer](item)
- switch off [lamp](item)
- switch off [laptop](item)
- turn my [laptop](item) off
- turn my [phone](item) off
- turn off [computer](item)
- turn off [lamp](item)
- turn off [laptop](item)
- turn off my [laptop](item)
- turn off my [phone](item)
- turn off the [desk lamp](item)
- turn off the [fan](item)
- turn the [ceiling fan](item) off
- turn the [computer](item) off
- turn the [desk lamp](item) off
- turn the [fan](item) off
- turn the [laptop](item) off
- turn the [light](item) off
- turn the [machine](item) off
- turn the [tablet](item) off

## intent:undo_last
- pretend i didn't do that
- undo
- undo last action
- undo last move
- undo last turn
- wait i didn't mean to do that

## intent:unlock
- can i unlock the [door](item)
- can i unlock the [door](item) using my key
- can you please unlock [door](item)
- can you unlock the [door](item)
- please unlock [door](item)
- please unlock [drawer](item)
- please unlock my [door](item)
- unlock door
- unlock drawer
- unlock my door
- unlock the [desk](item) drawer
- unlock the [desk](item) drawer now
- unlock the [door](item)
- unlock the [drawer](item)
- unlock the [drawer](item) now
