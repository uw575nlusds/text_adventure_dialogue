To use interactive shell with full bot:
rasa shell --model models/(latest model)

With just NLU to see intent classification/entity extraction:
rasa shell nlu

Test results from testing using test data split and model nlu-20200215-040940.tar.gz
