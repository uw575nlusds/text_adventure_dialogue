# How to Annotate With SLATE
0. Ensure SLATE has been installed via pip (see ``requirements.txt``).
1. Change your working directory to the slate directory.
    * ``cd slate``
2. Create a file of the same format described in ``./slate/examples/slate_example.txt``
    * The file created here is referred to as ``<original_file>`` in the rest of this documentation.
3. Preprocess file to separate intents and data.
    * ``python preprocess_for_slate.py <original_file>``
4. Annotate the newly created file that contains no intents.
    * ``./annotate.sh <original_file>.data``
    * See below for instructions on how to navigate this interface
5. Convert SLATE annotations to Rasa format.
    * This command takes four arguments:
    * ``python slate2rasa.py <original_file>.data <original_file>.intents <original_file>.data.annotations <new_rasa_file>``

    
## SLATE Keybindings
* Use the ``WASD`` keys to move the cursor between lines and tokens.
* Press ``e`` to annotate the highlighted token with the ``item`` label
* Press ``r`` to annotate the highlighted token with the ``area`` label
* Press ``t`` to annotate the highlighted token with the ``indirect_item`` label
* Press ``q`` to quit (results save automatically)
* For more keybinds you can inspect the ``slate.config`` file or SLATE documentation. However these keybindings describe all major functionality.
* For personalization the ``slate.config`` file can be freely modified to suit your tastes.

## SLATE Labels & Colors
* The ``item`` label is colored green by default
* The ``area`` label is colored blue by default
    - This may or may not be needed but it's better to over-annotate than under-annotate
* The ``indirect_item`` label is colored magenta
    - This is mostly unnecessary for most intents but for others it may allow for advanced functionality.
    - For example ``"I put the pen in the drawer."`` has ``item==pen`` and ``indirect_item==drawer``
* Labels and colors can be freely personalized in the ``slate.config`` file.