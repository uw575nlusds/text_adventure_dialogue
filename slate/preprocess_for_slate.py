from argparse import ArgumentParser
from re import split as re_split


def parse_args():
    """Parse CLI Arguments."""
    parser = ArgumentParser('Prepares intent-annotated data for use with the SLATE tool')
    parser.add_argument('data_path', type=str, help='Path to file containing one intent and data instance per line')
    parser.add_argument('--intent_ext', default='.intents', help='Suffix appended to newly created intents file')
    parser.add_argument('--data_ext', default='.data', help='Suffix appended to newly created data file')

    arguments = parser.parse_args()
    vargs = vars(arguments)
    print()
    for arg in vargs:
        print('{0:16} {1}'.format(arg, vargs[arg]))
    print()

    return arguments


if __name__ == '__main__':
    args = parse_args()

    # Read whitespace-delimited file containing intent-annotated data
    with open(args.data_path, 'r') as f:
        full_data = f.read()

    # Split data into intents and data
    intent_lines, data_lines = [], []
    for line in full_data.strip().split('\n'):
        split_line = re_split(r'(\s+)', line)  # allows for tracking unlimited whitespace
        intent_lines.append(split_line[0])
        data_lines.append(''.join(split_line[2:]))  # skip intent and first whitespace chunk

    # Write data to separate files
    with open('%s%s' % (args.data_path, args.intent_ext), 'w') as intent_f:
        for line in intent_lines:
            intent_f.write('%s\n' % line)
    with open('%s%s' % (args.data_path, args.data_ext), 'w') as data_f:
        for line in data_lines:
            data_f.write('%s\n' % line)
