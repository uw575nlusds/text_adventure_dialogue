from argparse import ArgumentParser
from ast import literal_eval
from collections import defaultdict
from json import dump as json_dump
from re import split as re_split


def parse_args():
    """Parse CLI Arguments."""
    parser = ArgumentParser('Converts SLATE annotations to Rasa annotations.')
    parser.add_argument('data_path', type=str, help='Path to text file containing one data instance per line')
    parser.add_argument('intent_path', type=str, help='Path to text file containing one intent label per line')
    parser.add_argument('slate_path', type=str, help='Path to text file containing SLATE annotations')
    parser.add_argument('rasa_path', type=str, help='Path to output Rasa data in JSON format')

    arguments = parser.parse_args()
    vargs = vars(arguments)
    print()
    for arg in vargs:
        print('{0:16} {1}'.format(arg, vargs[arg]))
    print()

    return arguments


def get_examples(text, intent, annotations):
    """Calculate Rasa representations for a single data instance."""
    entities = []
    tokens = text.strip().split()
    tokens_with_whitespace = re_split(r'(\s+)', text)  # allows for tracking unlimited whitespace
    for annotation in annotations:
        token_id, ws_token_id = annotation[0], annotation[0] * 2
        entity = annotation[1]
        value = tokens[token_id]
        start = sum([len(token) for token in tokens_with_whitespace[:ws_token_id]])
        end = start + len(value)
        entities.append({'start': start, 'end': end, 'entity': entity, 'value': value})
    return {'text': text, 'intent': intent, 'entities': entities}


def map_data_to_annotations(annotations):
    """Maps indexes of data instances to all of their associated annotations."""
    mapped_data = defaultdict(list)
    for row in annotations.split('\n'):
        row = [r.strip() for r in row.split('-')]
        if len(row) == 2:  # valid rows will always have 2 elements
            data_id, token_id = literal_eval(row[0])
            mapped_data[data_id].append((token_id, row[1].split(':')[1]))
    return mapped_data


def slate2rasa(data, intents, annotations):
    """Converts data instances with SLATE annotations to Rasa format."""
    data = data.strip().split('\n')
    intents = intents.strip().split('\n')
    mapped_data = map_data_to_annotations(annotations)
    examples = [get_examples(text, intent, mapped_data[i]) for i, (text, intent) in enumerate(zip(data, intents))]
    return {'rasa_nlu_data': {'common_examples': examples}}


if __name__ == '__main__':
    args = parse_args()

    # Read data file
    with open(args.data_path, 'r') as f:
        raw_data = f.read()
    # Read intents file
    with open(args.intent_path, 'r') as f:
        raw_intents = f.read()
    # Read SLATE annotations file
    with open(args.slate_path, 'r') as f:
        slate_annotations = f.read()
    # Write Rasa data
    with open(args.rasa_path, 'w') as f:
        json_dump(slate2rasa(raw_data, raw_intents, slate_annotations), f, indent=2, sort_keys=True)
