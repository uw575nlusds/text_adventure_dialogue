"""
Contains location logic.
"""

from entities import *
from inspect import getmembers, isclass

from logger import get_logger
log = get_logger()

# todo verify natural language names (cls.names)


def _construct_area_class_map():
    """Build a dictionary mapping string names (in natural language) to classes."""
    ic_map = {}
    class_list = [m[1] for m in getmembers(modules[__name__], isclass) if m[1].__module__ == 'location']
    for cl in class_list:
        if hasattr(cl, 'names'):
            for name in cl.names:
                ic_map[name] = cl
    return ic_map


def area2class(area_name):
    """Converts area names to class definitions."""
    area_name = area_name.lower()
    return _area_class_map[area_name] if area_name in _area_class_map else None


class Room(object):
    def __init__(self, script):
        self.areas = {EntranceArea(), DeskArea(), CouchArea(), EndTableArea()}
        self.script = script

    def get_area(self, area_class):
        areas = [area for area in self.areas if issubclass(type(area), area_class)]
        return [None] if len(areas) == 0 else areas

    def get_items(self, item_class=None, must_be_visible=False):
        items = []
        for area in self.areas:
            items.extend(area.get_items(item_class, must_be_visible))
            items = [i for i in items if i is not None]
            if item_class is not None and len(items) > 0:
                break
        return [None] if all(map(lambda x: x is None, items)) else items

    def introduce(self):
        log.info(self.script['INTENT_OK']['GoToIntent']['root'])
        log.info('')

    def move_x_from_y_to_z(self, entity_x, area_y, area_z):
        assert issubclass(entity_x, Item) and issubclass(area_y, Area) and issubclass(area_z, Area)
        x = area_y.get_items(item_class=entity_x, must_be_visible=False, pop=True)
        self.get_area(area_z)[0].put_item(x)
        log.debug('entity %s has been moved from %s to %s' % (entity_x, area_y, area_z))

    def player_start(self):
        return self.get_area(EntranceArea)[0]


class Area(object):
    names = []

    def __init__(self):
        self.items = set()

    def get_items(self, item_class=None, must_be_visible=False, pop=False):
        items = []
        for item in self.items:
            if item_class is None or isinstance(item, item_class):
                items.append(item)
            if isinstance(item, ContainerItem):
                if isinstance(item, HiddenContainerItem):
                    items.extend(item.get_items(item_class=item_class, respect_hidden=must_be_visible, pop=pop))
                else:
                    items.extend(item.get_items(item_class=item_class, pop=pop))
        if pop:
            for item in items:
                if item in self.items:
                    self.items.remove(item)
        return [None] if len(items) == 0 else items

    def get_state(self):
        return State.LOC_STATELESS

    def has_item(self, item):
        return False if self.get_items(item_class=item, must_be_visible=False)[0] is None else True

    def put_item(self, item):
        self.items.add(item)

    @classmethod
    def canonical_name(cls):
        return cls.names[0] if len(cls.names) > 0 else 'area'

    @staticmethod
    def is_lobby():
        """Can this Area be the player's current location?"""
        return False


class LobbyArea(Area):
    @staticmethod
    def is_lobby():
        return True


class EntranceArea(LobbyArea):
    names = ['entrance', 'door', 'door area', 'entrance area', 'start', 'rack', 'coat rack', 'coat-rack', 'jackets',
             'coat stand', 'stand', 'coat hanger', 'coats']

    def __init__(self):
        super().__init__()
        self.items = self.initial_items()

    @staticmethod
    def initial_items():
        door, jacket, phone, rack = Door(), Jacket(), Phone(), CoatRack()
        jacket.add_to_inventory(phone)
        return {door, jacket, rack}


class DeskArea(LobbyArea):
    names = ['desk', 'desk area', 'area near the desk']

    def __init__(self):
        super().__init__()
        self.items = self.initial_items()

    @staticmethod
    def initial_items():
        chair, drawer, laptop, lamp, papers, poster, usb = Chair(), Desk(), Laptop(), \
                                                           Lamp(), Papers(), Poster(), USB()
        drawer.add_to_inventory(usb)
        return {chair, drawer, laptop, lamp, papers, poster}


class CouchArea(LobbyArea):
    names = ['couch', 'couch area', 'area by the couch']

    def __init__(self):
        super().__init__()
        self.items = self.initial_items()

    @staticmethod
    def initial_items():
        backpack, couch, textbook, wine, wug = Backpack(), Couch(), Textbook(), Wine(), Wug()
        ceiling_fan, charger = CeilingFan(), Charger()
        backpack.add_to_inventory(textbook)
        ceiling_fan.add_to_inventory(charger)
        return {backpack, ceiling_fan, couch, wine, wug}

    def get_state(self):
        charger_visible = isinstance(self.get_items(item_class=Charger, must_be_visible=True)[0], Charger)
        return State.ITEM_ACTIVE if charger_visible else State.ITEM_DEACTIVE


class EndTableArea(LobbyArea):
    names = ['end table', 'end table area', 'table', 'table area', 'fountain']

    def __init__(self):
        super().__init__()
        self.items = self.initial_items()

    @staticmethod
    def initial_items():
        fountain, key_to_desk = Fountain(), KeyToDesk()
        fountain.add_to_inventory(key_to_desk)
        return {fountain}


_area_class_map = _construct_area_class_map()

