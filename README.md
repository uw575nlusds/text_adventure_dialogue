# Dialogue Systems for Text Adventure Games
Allow users to interact with text adventure games using natural language with rich syntax and vocabulary, utilizing a Rasa NLU model.  
A simple text adventure game is presented as a proof-of-concept for the accesibility and reusability of NLU in text adventure games.

## Rasa
* Our Rasa NLU model is trained with a ConverRT embeddings tokenizer/featurizer for intent classification and a CRF featurizer for entity classification
* Further information on Rasa pipelines can be found [here](https://rasa.com/docs/rasa/nlu/choosing-a-pipeline/)
* Our specific pipeline setup can be found in 'rasa_bot/config.yml'
* To fully understand the files in our 'rasa_bot' directory, check out the full Rasa documentation [here](https://rasa.com/docs/rasa/)
* To add more utterances to data:
    - Add annotated utterances to data file and retrain Rasa
    - Use Slate tool to rapidly annotate data. Slate can be set up using the instructions in 'slate/README.md'

## Game Concept
* 'This is it, your big day. You’ve finally finished your dissertation and it’s time to send it off to the committee. Now, where did you put it...?'
* Use natural language to explore your study, discover clues, and find your thesis
* Discover lingusit/adventure game easter eggs
* Find every way to end the game! (Hint: there's only one good way)

## Requirements
* Python 3.7.4
* Rasa 1.9.0
* See requirements.txt for full listing

## Instructions for Use
### Rasa NLU
* The Rasa NLU model can be run continously on a remote server, or run locally
* In our case, the code to interface with the server is in our game logic code, but a separate wrapper could be written to do so
* Specific instructions for interacting locally with a Rasa NLU server can be found [here](https://rasa.com/docs/rasa/nlu/using-nlu-only/)

### Text Adventure Game
* To start the game, run the following in a Unix/Linux/OSX terminal:
    - python3 game.py
* Further Specifications:
    - '--model' can be used to specify a specific model from 'rasa_bot/models/'
    - '--script' can be used to specify a specific script file other than 'script.json' (although only one script is currently available)
    - '--debug', if specified will print output from the NLU server to aid in debugging
* For debugging, you can also check game.log which is outputted live as the game is played

## Resources
* [Rasa](https://rasa.com/docs/)
* [Slate](https://github.com/jkkummerfeld/slate) 
