from textwrap import wrap
import logging
import sys


def get_logger():
    return logging.getLogger()


def initialize_logger(log_file='game.log', route_print=True):
    class QuietInfoFormatter(logging.Formatter):
        def __init__(self, quiet_info=True):
            super().__init__(fmt="%(asctime)s:%(levelname)s:%(message)s", datefmt=None, style='%')
            self.quietInfo = quiet_info

        def format(self, record):
            #print(record.msg)
            record.msg = '\n'.join(wrap(record.msg, width=100))
            og_format = self._style._fmt  # save original format
            if record.levelno == logging.INFO and self.quietInfo:
                self._style._fmt = '%(message)s'  # replace format with quiet info
            result = logging.Formatter.format(self, record)
            self._style._fmt = og_format  # restore original format
            return result

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)  # required or will only log WARNING or higher

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setFormatter(QuietInfoFormatter(quiet_info=True))
    stdout_handler.setLevel(logging.INFO)
    stderr_handler = logging.FileHandler(log_file, 'a')
    stderr_handler.setFormatter(QuietInfoFormatter(quiet_info=False))
    stderr_handler.setLevel(logging.DEBUG)

    if route_print:  # route all calls to print() to instead print to logger
        global print
        print = logging.debug

    logger.addHandler(stdout_handler)
    logger.addHandler(stderr_handler)
    return logger


def set_logger_to_debug_mode():
    log = get_logger()
    for h in log.handlers:
        h.setLevel(logging.DEBUG)


if __name__ == '__main__':
    """Testing logger. Don't actually use this method for anything meaningful."""
    initialize_logger()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

    log.debug('dbg')
    log.info('inf')
    log.warning('wrn')
    log.error('err')
    log.critical('crt')

    print('print to dbg')
