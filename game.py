"""
Contains high-level game logic.
"""

from logger import initialize_logger, set_logger_to_debug_mode
log = initialize_logger()

from argparse import ArgumentParser
from intents import IntentHandler, QuitCommand, UndoCommand
from location import *
from player import *
from states import State
from time import sleep
import json


# todo implement undo if you are feeling cheeky (more work than it might seem)

# todo remove undo intent, add call intent, add plug in intent [or enhance turn on intent]
# todo latest model instead of hardcoded?

class Game(object):

    quit_codes = {'q', 'quit'}
    undo_codes = {'u', 'undo'}

    def __init__(self, model, script):
        self.script = json.load(open(script, 'r'))
        self.room = Room(self.script)
        self.player = Player(self.room.player_start())
        self.state = State.GAME_OK
        self.intent_handler = IntentHandler(model, self.script)
        self.history = []  # todo list of Intents  # todo max length of history == ?

    """Game State Manipulation"""

    def change_player_location(self, area_class):
        new_area = self.room.get_area(area_class)[0]
        self.player.location = new_area
        log.debug('player is now in %s' % new_area)

    def knock_charger_down(self):
        self.room.get_items(item_class=Charger,)
        log.debug('charger has been knocked down')

    def put_item_in_player_inventory(self, item_class):
        item = self.player.location.get_items(item_class, must_be_visible=True, pop=True)[0]
        self.player.inventory.append(item)
        log.debug('%s has been taken from %s and put in player\'s inventory' % (item, self.player.location))

    def visible_items(self, item_class=None):
        items = self.room.get_items(item_class=item_class, must_be_visible=True)
        return items

    """Game Flow Manager"""

    def interpret(self):
        raw_input = input('> ').replace('"', '\\"').strip() or 'look'  # preempts an input bug during program sleep
        log.debug('player entered: \'%s\'' % raw_input)
        if raw_input.lower() in Game.quit_codes:
            return QuitCommand()
        elif raw_input.lower() in Game.undo_codes:
            return UndoCommand()
        else:
            return self.intent_handler.interpret(raw_input)

    def introduce(self):
        """Print text which explains the scenario to the player."""
        log.info(self.script['INTENT_OK']['GoToIntent']['lobby'])
        log.info('')

    def load(self, poll_freq=1.0):
        """
        Shows loading screen while waiting for NLU server to initialize.

        :param poll_freq: float number of seconds between server polls while waiting
        :return:
        """
        self.intent_handler.init()
        sec_to_load = 0.0
        while not self.intent_handler.active():
            print('\rLoading ... (%d)' % sec_to_load, end='', flush=True)
            sec_to_load += poll_freq
            sleep(poll_freq)
        print('\r', end='', flush=True)

    def play(self):
        self.introduce()
        self.room.introduce()
        while self.state == State.GAME_OK:
            intent = self.interpret()
            valid_code = intent.validate(self)
            log.debug(valid_code.name)  # todo instead of returning valid code, track it implicitly in each Intent as a field
            if valid_code == State.INTENT_OK:
                intent.execute(self)  # todo validity assertion in execute?
            log.info(intent.report(valid_code, self))

    def report_game_results(self):
        if self.state == State.GAME_WIN:
            log.info(self.script['GAME_WIN'][''])
        elif self.state == State.GAME_LOSE_ALCOHOL:
            log.info(self.script['GAME_LOSE_ALCOHOL'][''])
        elif self.state == State.GAME_LOSE_LEFT:
            log.info(self.script['GAME_LOSE_LEFT'][''])
        elif self.state == State.GAME_LOSE_WEB:
            log.info(self.script['GAME_LOSE_WEB'][''])
        elif self.state == State.GAME_QUIT:
            log.info(self.script['GAME_QUIT'][''])
        else:
            raise ValueError('Game should never end in the %s state! Something is wrong.' % State(self.state))

    def teardown(self):
        self.intent_handler.teardown()


def parse_args():
    parser = ArgumentParser('Play a game!')
    parser.add_argument('--model', default='./rasa_initial_bot/models/nlu-20200319-122919.tar.gz',
                        help='Location to Rasa model for NLU parsing.')
    parser.add_argument('--script', default='./script.json',
                        help='Location to JSON file containing script information.')
    parser.add_argument('--debug', action='store_true',
                        help='Running the program in debug mode will print more messages to your screen.')
    arguments = parser.parse_args()
    return arguments


if __name__ == '__main__':
    args = parse_args()
    if args.debug:
        set_logger_to_debug_mode()
    log.debug('game begin')
    game = Game(args.model, args.script)
    try:
        game.load()
        game.play()
        game.report_game_results()
    except:
        log.exception('Error: ')  # allows to log to file
        raise
    finally:
        game.teardown()
        log.debug('game quit')
