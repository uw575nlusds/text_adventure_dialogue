"""
Contains player logic.
"""

from entities import Papers, Phone, Poster, Textbook
from logger import get_logger
log = get_logger()


class Player(object):
    def __init__(self, location):
        self.discovered_items = set()
        self.location = location
        #self.history = []
        self.inventory = []
        self.sitting_on = None

    def discover(self, item_class):
        self.discovered_items.add(item_class)

    def get_items(self, item_class=None):
        if item_class is None:
            return self.inventory if len(self.inventory) > 0 else [None]
        items = [i for i in self.inventory if isinstance(i, item_class)]
        return [None] if all(map(lambda x: x is None, items)) else items

    def has_item(self, item):
        return True if item in [type(x) for x in self.inventory] else False

    def in_area(self, area_class):
        return False if area_class is None else isinstance(self.location, area_class)

    def is_seated(self, item_class=None):
        if item_class is None:
            return False if self.sitting_on is None else True
        else:
            return True if isinstance(self.sitting_on, item_class) else False

    def knows_address(self):
        return True if Phone in self.discovered_items else False

    def knows_password(self):
        return True if all([(i in self.discovered_items) for i in {Papers, Poster, Textbook}]) else False

    def sit(self, item):
        self.sitting_on = item
