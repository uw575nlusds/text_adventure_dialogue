"""
Contains logic for parsing user input into actionable intents.
"""

from entities import *
from location import area2class, CouchArea
from states import State
from subprocess import DEVNULL, PIPE, Popen, run
import json

from logger import get_logger
log = get_logger()


# todo low-priority: abstract game logic (for example the execute() methods might be more specific than they should)
# todo low-priority: improve efficiency by slightly reworking the intent constructor to do some preprocessing only once


class IntentHandler(object):
    def __init__(self, model_path, script):
        self.model_path = model_path
        self.rasa = None
        self.script = script
        self.class_map = {
            'go_to_destination': GoToIntent,
            'put_item_in_inventory': TakeItemIntent,
            'look_at_item': LookIntent,
            'turn-on': TurnOnIntent,
            'turn_off': TurnOffIntent,
            'lock': LockIntent,
            'unlock': UnlockIntent,
            'open': OpenIntent,
            'close': CloseIntent,
            'sit': SitIntent,
            'get_up_off_of': GetUpIntent,
            'drink': DrinkIntent,
            'call': CallIntent,
            'enter_password': EnterPasswordIntent,
            'browse_web': BrowseWebIntent,
            'hug': HugIntent
        }  # not static because of scoping weirdness

    def active(self):
        """Checks to see if the Rasa server connection is active."""
        if self.rasa is None:
            return False
        err = run('curl localhost:5005/model/parse -d \'{"text":"' + 'hello' + '"}\'',
                  stdout=PIPE, stderr=PIPE, shell=True, universal_newlines=True).stderr
        return True if ('Failed to connect to localhost port 5005: Connection refused' not in err) else False

    def construct_intent_by_name(self, name, cmd_result):
        """Constructs an Intent."""
        try:
            intent = self.class_map[name](cmd_result, self.script)
        except KeyError:
            intent = Intent(cmd_result, self.script)  # intent with unknown functionality ('None' in Rasa)
        return intent

    def init(self):
        """Initializes the Rasa server connection."""
        self.rasa = Popen('rasa run --enable-api -m %s' % self.model_path,
                          shell=True, stdout=DEVNULL, stderr=DEVNULL)

    def interpret(self, command):
        """Interprets a user's raw-text command using the Rasa server connection."""
        if self.rasa.poll() is None:
            command = 'curl localhost:5005/model/parse -d \'{"text":"' + command + '"}\''  # todo hardcoded port
            result = json.loads(run(command, shell=True, stderr=DEVNULL, stdout=PIPE, universal_newlines=True).stdout)
            log.debug('intent: %s' % result['intent'])
            log.debug('entities: %s' % result['entities'])
            log.debug('next highest intent: %s' % result['intent_ranking'][0])
            return self.construct_intent_by_name(result['intent']['name'], result)
        else:
            raise ChildProcessError('Cannot contact Rasa server')

    def teardown(self):
        """Safely closes connections."""
        if self.rasa is not None:
            self.rasa.kill()


class Command(object):
    def execute(self, game):
        """Applies the effects of this intent to the Game."""
        pass

    def validate(self, game):
        """Identifies what obstacles, if any, are preventing the intent from being performed."""
        return State.INTENT_OK  # todo do any universal validation that must be true for all intents. confidence score?

    # todo command has no report function, perhaps?
    def report(self, valid_code, game):
        """Print text to the user which describes the result of this intent, whether it succeeded or failed."""
        report = 'Report is not being overridden.'
        print(report)


class Intent(Command):

    def __init__(self, cmd_result, script):
        # todo item/area/indirect_item class fields to prevent redundancy?
        result_dict = json.loads(cmd_result) if isinstance(cmd_result, str) else cmd_result
        self.intent = result_dict['intent']
        self.entities = [(e['entity'], e['value']) for e in result_dict['entities']]

        # todo if more than one item/area: keep the one with the highest confidence
        self.item = [v for e, v in self.entities if e == 'item']
        self.area = [v for e, v in self.entities if e == 'area']
        self.indirect_item = [v for e, v in self.entities if e == 'indirect_item']
        self.item = self.item[0] if len(self.item) > 0 else ''
        self.area = self.area[0] if len(self.area) > 0 else ''
        self.indirect_item = self.indirect_item[0] if len(self.indirect_item) > 0 else ''

        self.script = script

    def execute(self, game):
        raise NotImplementedError('Unknown functionality cannot be executed.')

    def validate(self, game):
        valid = [super().validate(game)]
        if type(self) == Intent:
            valid.append(State.INTENT_ERR_INTENT_NOT_EXIST)
        return max(valid)

    def report(self, valid_code, game):
        return self.script['INTENT_ERR_INTENT_NOT_EXIST']['']['']

    @staticmethod
    def assemble_report(game, report_dict, objects, item_first=True):
        # todo fix this abomination of a function. it hurts my soul
        if len(objects) == 0:  # pad with empty strings to prevent index errors
            objects = ('', '')
        elif len(objects) == 1:
            objects = (objects[0], '')
        if item_first:
            objs = [o for o in map(lambda x: item2class(x), objects) if o is not None]
            if len(objs) == 0:
                objs = [o for o in map(lambda x: area2class(x), objects) if o is not None]
        else:
            objs = [o for o in map(lambda x: area2class(x), objects) if o is not None]
            if len(objs) == 0:
                objs = [o for o in map(lambda x: item2class(x), objects) if o is not None]
        obj_class = objs[0] if len(objs) > 0 else None
        obj_class_str = obj_class.__name__ if obj_class is not None else None
        if obj_class is Couch:  # todo bad bad bad
            obj_class = CouchArea
            obj_class_str = obj_class.__name__
        obj_canon = obj_class.canonical_name() if obj_class is not None else None
        if obj_canon is None:
            obj_canon = objects[1] if objects[0] == '' else objects[1]
            if obj_canon == '':
                obj_canon = '???'
        try:  # todo this try block is too broad (TypeErrors thrown for reasons other than those anticipated)
            report = report_dict[obj_class_str] if obj_class_str in report_dict else report_dict[''] % obj_canon
            if isinstance(report, dict):
                instance = game.room.get_items(item_class=obj_class)[0]
                if instance is None:
                    instance = game.room.get_area(area_class=obj_class)[0]
                    if instance is None:
                        instance = game.player.get_items(item_class=obj_class)[0]
                state = instance.get_state().name
                report = report[state]
                if obj_class is Desk and state is State.ITEM_ACTIVE.name and game.room.get_items(USB)[0] is None:
                    report = '.'.join(report.split('.')[0:-1]) + '.'  # todo desperation can drive you to dark places
        except TypeError:
            report = report_dict['']
        except KeyError:
            report = ''
        return report

    """Vocabulary-Based Validation"""

    @staticmethod
    def validate_area_exists(area):
        return State.INTENT_OK if area is not None else State.INTENT_ERR_AREA_NOT_EXIST

    @staticmethod
    def validate_item_exists(item):
        return State.INTENT_OK if item is not None else State.INTENT_ERR_ITEM_NOT_EXIST

    """Player-Based Validation"""

    @staticmethod
    def validate_player_not_in_area(game, area):
        return State.INTENT_OK if not game.player.in_area(area) else State.INTENT_ERR_PLAYER_IN_AREA

    @staticmethod
    def validate_player_has_item(game, item):
        return State.INTENT_OK if game.player.has_item(item) else State.INTENT_ERR_PLAYER_NOT_HAS_ITEM

    @staticmethod
    def validate_player_not_has_item(game, item):
        return State.INTENT_OK if not game.player.has_item(item) else State.INTENT_ERR_PLAYER_HAS_ITEM

    @staticmethod
    def validate_player_sitting(game, item_class=None):
        return State.INTENT_OK if game.player.is_seated(item_class) else State.INTENT_ERR_PLAYER_NOT_SEATED

    @staticmethod
    def validate_player_not_sitting(game):
        return State.INTENT_OK if not game.player.is_seated() else State.INTENT_ERR_PLAYER_SEATED

    @staticmethod
    def validate_player_knows_password(game):
        return State.INTENT_OK if game.player.knows_password() else State.INTENT_ERR_PUZZLE_UNSOLVED

    """Class-Based Validation"""

    @staticmethod
    def validate_item_grabbable(item):
        return State.INTENT_OK if item is not None and item.can_be_grabbed() else State.INTENT_ERR_NONFUNCTION

    @staticmethod
    def validate_item_lockable(item):
        return State.INTENT_OK if item is not None and item.can_be_locked() else State.INTENT_ERR_NONFUNCTION

    @staticmethod
    def validate_item_electronic(item):
        return State.INTENT_OK if item is not None and item.is_electronic() else State.INTENT_ERR_NONFUNCTION

    @staticmethod
    def validate_item_password_protected(item):
        return State.INTENT_OK if item is not None and item.is_password_protected() else State.INTENT_ERR_NONFUNCTION

    @staticmethod
    def validate_item_openable(item):
        return State.INTENT_OK if item is not None and item.can_be_opened() else State.INTENT_ERR_NONFUNCTION

    @staticmethod
    def validate_item_sittable(item):
        return State.INTENT_OK if item is not None and item.can_be_sat_upon() else State.INTENT_ERR_NONFUNCTION

    @staticmethod
    def validate_item_drinkable(item):
        return State.INTENT_OK if item is not None and item.can_be_drunk() else State.INTENT_ERR_NONFUNCTION

    @staticmethod
    def validate_can_move_to_area(area):
        # todo possibly there is a better state to return: INTENT_ERR_AREA_NOT_MOVE_TO; or, NONFUNCTION?
        return State.INTENT_OK if area is not None and area.is_lobby() else State.INTENT_ERR_AREA_NOT_EXIST

    @staticmethod
    def validate_item_active(game, item, activity):
        #active = len(game.visible_items(item)[0].active_traits) > 0
        if item is None:
            return State.INTENT_ERR_ITEM_NOT_EXIST
        item_instance = game.room.get_items(item_class=item, must_be_visible=False)[0]
        if item_instance is None:
            item_instance = game.player.get_items(item_class=item)[0]
        active = activity in item_instance.active_traits if item_instance is not None else False
        return State.INTENT_OK if active else State.INTENT_ERR_ITEM_DEACTIVE

    @staticmethod
    def validate_item_deactive(game, item, activity):
        #active = len(game.visible_items(item)[0].active_traits) > 0
        if item is None:
            return State.INTENT_ERR_ITEM_NOT_EXIST
        item_instance = game.room.get_items(item_class=item, must_be_visible=False)[0]
        active = activity in item_instance.active_traits if item_instance is not None else False
        return State.INTENT_OK if not active else State.INTENT_ERR_ITEM_ACTIVE

    @staticmethod
    def validate_item_unlocked(game, item):  # todo incorporate into generic deactive method
        #unlocked = LockableItem not in game.visible_items(item)[0].active_traits
        #locked_items = [i for i in game.visible_items() if isinstance(i, LockableItem)]
        if item is None:
            return State.INTENT_ERR_ITEM_NOT_EXIST
        item_instance = game.room.get_items(item_class=item, must_be_visible=False)[0]
        unlocked = LockableItem not in item_instance.active_traits if item_instance is not None else False
        return State.INTENT_OK if unlocked else State.INTENT_ERR_ITEM_LOCKED

    @staticmethod
    def validate_item_needs_authentication(game, item):
        if item is None:
            return State.INTENT_ERR_ITEM_NOT_EXIST
        item_instance = game.room.get_items(item_class=item, must_be_visible=False)[0]
        authenticated = PasswordProtectedItem not in item_instance.active_traits if item_instance is not None else False
        return State.INTENT_OK if not authenticated else State.INTENT_ERR_ITEM_AUTHENTICATED

    """Area-Based Validation"""

    @staticmethod
    def validate_item_visible(game, item):
        if item is None:
            return State.INTENT_ERR_ITEM_NOT_EXIST
        #return State.INTENT_OK if game.player_can_see_item(item) else State.INTENT_ERR_ITEM_NOT_EXIST
        visible = game.visible_items(item_class=item)[0] is not None
        return State.INTENT_OK if visible else State.INTENT_ERR_ITEM_NOT_EXIST

        #return State.INTENT_OK if len(game.visible_items(item)) > 0 else State.INTENT_ERR_ITEM_NOT_EXIST

    @staticmethod
    def validate_player_can_reach_item(game, item):
        if item is None:
            return State.INTENT_ERR_ITEM_NOT_EXIST
        if game.player.location.has_item(item) or game.player.has_item(item):
            return State.INTENT_OK
        else:
            return State.INTENT_ERR_NOT_REACH_ITEM
        #return State.INTENT_OK if game.player.location.has_item(item) else State.INTENT_ERR_NOT_REACH_ITEM


class GoToIntent(Intent):
    def execute(self, game):
        area_class = area2class(self.area)
        area_class = area2class(self.item) if area_class is None else area_class
        game.change_player_location(area_class)
        log.debug('player has moved to area %s' % area_class)

    def validate(self, game):
        """Verify the room can be moved to, and that the player is not currently in the room."""
        valid = [super().validate(game)]  # valid will always be a state code
        area_class = area2class(self.area)
        if area_class is None:
            area_class = area2class(self.item)
        valid.append(Intent.validate_area_exists(area_class))
        if area_class is None:
            return max(valid)
        valid.append(Intent.validate_player_not_in_area(game, area_class))
        valid.append(Intent.validate_player_not_sitting(game))
        valid.append(Intent.validate_can_move_to_area(area_class))
        return max(valid)  # all error states are larger than the ok state; pick an arbitrary error, if any

    def report(self, valid_code, game):
        #report = 'You move closer to the %s.' % self.area
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__],
                                    (self.area, self.item), item_first=False)  # todo similar code for most intents


class TakeItemIntent(Intent):
    def execute(self, game):
        item = item2class(self.item)
        game.put_item_in_player_inventory(item)
        log.debug('item %s has been added to player\'s inventory' % item)
        log.debug('player\'s inventory is now: %s' % game.player.inventory)

    def validate(self, game):
        """Verify the item can be taken, that it has not already been taken, and that the user can reach the item."""
        valid = [super().validate(game)]  # valid will always be a state code
        item_class = item2class(self.item)
        valid.append(Intent.validate_item_exists(item_class))
        valid.append(Intent.validate_player_not_has_item(game, item_class))
        if valid[-1] is State.INTENT_OK:
            valid.append(Intent.validate_player_can_reach_item(game, item_class))
            valid.append(Intent.validate_item_visible(game, item_class))
            valid.append(Intent.validate_item_grabbable(item_class))
        return max(valid)  # all error states are larger than the ok state; pick an arbitrary error, if any

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class LookIntent(Intent):
    def execute(self, game):
        item_class = item2class(self.item)
        if item_class is None:
            item_class = item2class(self.area)
        item = game.room.get_items(item_class)[0]
        if item is None:
            item = game.player.get_items(item_class)[0]
        if isinstance(item, Fountain) or isinstance(item, Jacket):
            item.contents_discovered = True
        if type(item) in {Papers, Poster, Textbook}:
            game.player.discover(type(item))


    def validate(self, game):
        """Verify the player can see or examine the item."""
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class is not None:
            valid.append(Intent.validate_item_exists(item_class))
            valid.append(Intent.validate_player_has_item(game, item_class))
            if valid[-1] is not State.INTENT_OK:
                valid[-1] = Intent.validate_item_visible(game, item_class)
        else:
            area_class = area2class(self.area)
            if area_class is not None:
                valid.append(Intent.validate_area_exists(area_class))

        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item, self.area))


class TurnOnIntent(Intent):
    def __init__(self, cmd_result, script):
        super().__init__(cmd_result, script)
        self.first_time_turn_on = False

    """Verify the item can be turned on, that it is not currently turned on, and that the player can reach it."""
    def execute(self, game):
        item = game.player.location.get_items(item_class=item2class(self.item), must_be_visible=True)[0]
        if item is None:
            item = game.player.get_items(item_class=item2class(self.item))[0]
        if isinstance(item, CeilingFan):
            if not item.has_been_activated:
                self.first_time_turn_on = True
            item.activate(ElectronicItem)
        elif isinstance(item, Laptop) or isinstance(item, Charger):
            item = game.player.location.get_items(item_class=Laptop, must_be_visible=True)[0]
            if game.player.has_item(Charger):
                item.activate(ElectronicItem)
        else:
            item.activate(ElectronicItem)
        log.debug('%s has been turned on (traits: %s)' % (item, item.active_traits))

    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class == Charger:
            item_class = Laptop
        if item_class == Laptop:
            valid.append(Intent.validate_player_has_item(game, Charger))
        elif item_class is not None:
            valid.append(Intent.validate_item_exists(item_class))
            valid.append(Intent.validate_player_has_item(game, item_class))
            if valid[-1] is not State.INTENT_OK:
                valid[-1] = Intent.validate_item_visible(game, item_class)
        valid.append(Intent.validate_item_electronic(item_class))
        valid.append(Intent.validate_item_deactive(game, item_class, ElectronicItem))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        report = self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))
        item_class = item2class(self.item)
        if item_class is CeilingFan and valid_code is State.INTENT_OK:
            item = game.player.location.get_items(item_class=CeilingFan, must_be_visible=True)[0]
            #if game.room.get_items(CeilingFan)[0].is_hidden():
            #if item.has_been_activated():
            if valid_code is State.INTENT_OK and not self.first_time_turn_on:
                report = '.'.join(report.split('.')[0:-1]) + '.'  # todo desperation can drive you to dark places
        return report


class TurnOffIntent(Intent):
    def execute(self, game):
        item = game.player.location.get_items(item_class=item2class(self.item), must_be_visible=True)[0]
        if item is None:
            item = game.player.get_items(item_class=item2class(self.item))[0]
        item.deactivate(ElectronicItem)
        log.debug('%s has been turned off (traits: %s)' % (item, item.active_traits))

    """Verify the item can be turned off, that it is not currently turned off, and that the player can reach it."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class is not None:
            valid.append(Intent.validate_item_exists(item_class))
            valid.append(Intent.validate_player_has_item(game, item_class))
            if valid[-1] is not State.INTENT_OK:
                valid[-1] = Intent.validate_item_visible(game, item_class)
        #valid.append(Intent.validate_item_exists(item_class))
        #valid.append(Intent.validate_item_visible(game, item_class))
        valid.append(Intent.validate_item_electronic(item_class))
        valid.append(Intent.validate_item_active(game, item_class, ElectronicItem))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class LockIntent(Intent):
    def execute(self, game):
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)
        item = game.player.location.get_items(item_class=item_class, must_be_visible=True)[0]
        item.activate(LockableItem)
        log.debug('%s has been locked (traits: %s)' % (item, item.active_traits))

    """Verify the item can be locked, that it is not currently locked, and that the player can reach it."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)

        valid.append(Intent.validate_item_exists(item_class))
        valid.append(Intent.validate_item_visible(game, item_class))
        valid.append(Intent.validate_item_lockable(item_class))
        valid.append(Intent.validate_item_deactive(game, item_class, LockableItem))
        valid.append(Intent.validate_item_deactive(game, item_class, OpenableItem))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item, self.area))


class UnlockIntent(Intent):
    def execute(self, game):
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)
        item = game.player.location.get_items(item_class=item_class, must_be_visible=True)[0]
        item.deactivate(LockableItem)
        log.debug('%s has been unlocked (traits: %s)' % (item, item.active_traits))

    """Verify the item can be unlocked, that it is not currently unlocked, and that the player can reach it."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)

        valid.append(Intent.validate_item_exists(item_class))
        valid.append(Intent.validate_item_visible(game, item_class))
        valid.append(Intent.validate_item_lockable(item_class))
        key_class = None
        if valid[-1] != State.INTENT_ERR_NONFUNCTION:
            key_class = game.room.get_items(item_class=item_class)[0].key
        valid.append(Intent.validate_item_active(game, item_class, LockableItem))
        valid.append(Intent.validate_player_has_item(game, key_class))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item, self.area))


class OpenIntent(Intent):
    def execute(self, game):
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)
        item = game.player.location.get_items(item_class=item_class, must_be_visible=True)[0]
        item.activate(OpenableItem)
        log.debug('%s has been opened (traits: %s)' % (item, item.active_traits))

    """Verify the item can be opened, that it is not currently opened, and that the player can reach it."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)

        valid.append(Intent.validate_item_exists(item_class))
        valid.append(Intent.validate_item_visible(game, item_class))
        valid.append(Intent.validate_item_unlocked(game, item_class))
        valid.append(Intent.validate_item_openable(item_class))
        #valid.append(Intent.validate_item_deactive(game, item_class, LockableItem))
        valid.append(Intent.validate_item_deactive(game, item_class, OpenableItem))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item, self.area))


class CloseIntent(Intent):
    def execute(self, game):
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)
        item = game.player.location.get_items(item_class=item_class, must_be_visible=True)[0]
        item.deactivate(OpenableItem)
        log.debug('%s has been closed (traits: %s)' % (item, item.active_traits))

    """Verify the item can be closed, that it is not currently closed, and that the player can reach it."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class is None:
            item_class = area2class(self.item)
            if item_class is None:
                item_class = item2class(self.area)
                if item_class is None:  # i cried too
                    item_class = area2class(self.area)

        valid.append(Intent.validate_item_exists(item_class))
        valid.append(Intent.validate_item_visible(game, item_class))
        valid.append(Intent.validate_item_openable(item_class))
        valid.append(Intent.validate_item_active(game, item_class, OpenableItem))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item, self.area))


class SitIntent(Intent):
    def execute(self, game):
        item_class = item2class(self.item)
        if item_class is None:
            item_class = Chair
        item = game.player.location.get_items(item_class=item_class, must_be_visible=True)[0]
        game.player.sitting_on = item
        log.debug('player is now sitting on %s' % item)

    """Verify the item can be sat upon, that the player is not currently sitting, and that the player can reach it."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        if item_class is None:
            valid.append(Intent.validate_player_can_reach_item(game, Chair))
            self.item = Chair.canonical_name()
        else:
            valid.append(Intent.validate_item_exists(item_class))
            valid.append(Intent.validate_item_visible(game, item_class))
            valid.append(Intent.validate_player_can_reach_item(game, item_class))
            valid.append(Intent.validate_item_sittable(item_class))
        valid.append(Intent.validate_player_not_sitting(game))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class GetUpIntent(Intent):
    def execute(self, game):
        game.player.sitting_on = None
        log.debug('player is now sitting on nothing')

    """Verify the item can be sat upon, and that the player is currently sitting on it."""
    def validate(self, game):
        valid = [super().validate(game)]
        valid.append(Intent.validate_player_sitting(game))
        player_seat = game.player.sitting_on
        if player_seat is not None:
            self.item = player_seat.canonical_name()
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class DrinkIntent(Intent):
    def execute(self, game):
        item = game.player.location.get_items(item_class=item2class(self.item), must_be_visible=True)[0]
        item.activate(DrinkableItem)
        log.debug('%s has been drank (traits: %s)' % (item, item.active_traits))
        if isinstance(item, Wine):
            game.state = State.GAME_LOSE_ALCOHOL

    """Verify the item can be drunk, that the item is not empty, and that the player can reach it."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)  # todo assert is not None
        valid.append(Intent.validate_item_exists(item_class))
        valid.append(Intent.validate_item_visible(game, item_class))
        valid.append(Intent.validate_item_drinkable(item_class))
        valid.append(Intent.validate_item_deactive(game, item_class, DrinkableItem))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class CallIntent(Intent):
    def execute(self, game):
        game.player.discover(Phone)

    def validate(self, game):
        valid = [super().validate(game)]
        valid.append(Intent.validate_player_has_item(game, Phone))
        valid.append(Intent.validate_item_active(game, Phone, ElectronicItem))
        self.item = Phone.canonical_name()
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class EnterPasswordIntent(Intent):
    def execute(self, game):
        self.item = Laptop.canonical_name()
        item = game.room.get_items(item_class=Laptop)[0]
        item.deactivate(PasswordProtectedItem)

    """Verify the item takes a password, that the item is turned on, that the player knows correct the password, and
    that the player can reach the item."""
    def validate(self, game):
        valid = [super().validate(game)]
        # todo usb
        valid.append(Intent.validate_player_sitting(game, Chair))
        valid.append(Intent.validate_item_password_protected(Laptop))
        valid.append(Intent.validate_player_can_reach_item(game, Laptop))
        valid.append(Intent.validate_item_active(game, Laptop, ElectronicItem))
        #valid.append(Intent.validate_item_active(game, Laptop, PasswordProtectedItem))
        valid.append(Intent.validate_item_needs_authentication(game, Laptop))
        valid.append(Intent.validate_player_knows_password(game))
        self.item = Laptop.canonical_name()
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class BrowseWebIntent(Intent):
    def execute(self, game):
        if game.player.knows_address() and game.player.has_item(USB):
            game.state = State.GAME_WIN
        else:
            game.state = State.GAME_LOSE_WEB

    """Verify the item is on, that it has internet access, and that the player can reach the item."""
    def validate(self, game):
        valid = [super().validate(game)]
        # player sitting at desk with turned-on computer OR player using turned-on mobile device
        valid.append(Intent.validate_player_sitting(game, Chair))
        valid.append(Intent.validate_player_can_reach_item(game, Laptop))
        valid.append(Intent.validate_item_active(game, Laptop, ElectronicItem))
        valid.append(Intent.validate_item_deactive(game, Laptop, PasswordProtectedItem))
        self.item = Laptop.canonical_name()
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class HugIntent(Intent):
    def execute(self, game):
        pass  # actually nothing changes

    """Verify that the player can reach the item."""
    def validate(self, game):
        valid = [super().validate(game)]
        item_class = item2class(self.item)
        valid.append(Intent.validate_item_exists(item_class))
        valid.append(Intent.validate_item_visible(game, item_class))
        valid.append(Intent.validate_player_can_reach_item(game, item_class))
        return max(valid)

    def report(self, valid_code, game):
        return self.assemble_report(game, self.script[valid_code.name][type(self).__name__], (self.item,))


class QuitCommand(Command):
    def execute(self, game):
        game.state = State.GAME_QUIT

    def validate(self, game):
        valid = [super().validate(game)]
        return max(valid)

    def report(self, valid_code, game):  # not static because may need to access fields for message customization?
        report = 'Game over.'
        return report


class UndoCommand(Command):
    def execute(self, game):
        raise NotImplementedError('Undo functionality not implemented.')

    def validate(self, game):
        valid = [super().validate(game)]
        valid.append(State.INTENT_ERR_NONFUNCTION)
        return max(valid)

    def report(self, valid_code, game):
        report = 'Previous action revoked.'
        return report
